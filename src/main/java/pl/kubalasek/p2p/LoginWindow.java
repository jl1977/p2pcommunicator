package pl.kubalasek.p2p;
/**
 * Prosty komunikator P2P
 * @author Jakub Lasek
 * @version 1.0
 */
import pl.kubalasek.p2p.ui.Labels;
import pl.kubalasek.p2p.ui.Menu;
import pl.kubalasek.p2p.ui.TextField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Klasa LoginWindow
 *
 *
 */

public class LoginWindow extends JFrame implements ActionListener {
    Menu menu = new Menu(this);
    Labels label = new Labels();
    TextField textField = new TextField();
    LoginWindow loginWindow = this;

    /**
     * Konstruktor klasy LoginWindow
     *
     **/

    public LoginWindow(){
        setTitle("Peer to peer communicator");
        setSize(485,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setVisible(true);
        add(label.getConfirmUsernameAndPort());
        add(textField.getUsernameAndPort());

        textField.getUsernameAndPort().addActionListener(registerAction);
    }



    /**
     * Przechwytujemy kliknięcie enter na textfield usernameAndPort
     *
     * */
    Action registerAction = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try {
                MainWindow main = new MainWindow(textField.getUsernameAndPort().getText());
                main.setVisible(true);
                loginWindow.setVisible(false);
            }catch (Exception exception){
                System.out.println(exception);
            }
        }
    };
    /**
     * Klasa implementująca interfejs ActionListener musi być deklarowana jako abstrakcyjna
     * albo implementować metode abstrakcyjną actionPerformed
     * */
    @Override
    public void actionPerformed(ActionEvent e) { }



    public static void main(String[] args) {
        LoginWindow mainWindow = new LoginWindow();
    }

}
