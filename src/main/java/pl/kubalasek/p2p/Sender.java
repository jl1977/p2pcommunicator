package pl.kubalasek.p2p;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;
public class Sender extends Thread {
    private ServerSocket serverSocket;
    private Set<SenderThread> senderThreads = new HashSet<SenderThread>();
    /**
     * Konstruktor klasy Sender
     * @param portNumb
     *        Parametr przekazywany z configa wykorzystywany do
     *        Używany do inicjalizacji serverSocket
     *
     * */
    public Sender(String portNumb) throws IOException {
        serverSocket = new ServerSocket(Integer.valueOf(portNumb));
    }
    /**
     * Uruchmienie SenderThread dla każdego peer'a
     * Dodajemy SenderThread do seta SenderThreads
     * Wywołujemy metodę start() dla każdego SenderThread
     * w klasie SenderThread.java wywoła run()
     * */
    public void run() {
        try {
            while (true) {
                SenderThread senderThread = new SenderThread(serverSocket.accept(), this);
                senderThreads.add(senderThread);
                senderThread.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void sendMessage(String message) {
        try {
            senderThreads.forEach(t -> t.getPrintWriter().println(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Set<SenderThread> getSenderThreads() {
        return senderThreads;
    }
}