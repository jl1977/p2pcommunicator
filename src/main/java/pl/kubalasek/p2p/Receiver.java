package pl.kubalasek.p2p;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import javax.json.Json;
import javax.json.JsonObject;
public class Receiver extends Thread {
    private BufferedReader bufferedReader;

    /**
     * Konstruktor klasy Receiver
     *
     * */
    public Receiver(Socket socket) throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    /**
     * Uruchomienie wątku
     * Tworzymy obiekt typu JSON odczytujący dane z bufora
     * Dane z bufora zamieniane są na String
     * Z JSON'a odczytujemy wartość atrybutu username i message
     * Dodajemy do pola konwersacji w glownym oknie programu
     * */
    public void run() {
        boolean flag = true;
        while (flag) {
            try {
                JsonObject jsonObject = Json.createReader(bufferedReader).readObject();
                System.out.println(jsonObject.toString());
                if (jsonObject.containsKey("username")) {
                    System.out.println("[" + jsonObject.getString("username") + "]: " + jsonObject.getString("message"));

                    MainWindow.appendNewMessage("<-"+"[" + jsonObject.getString("username") + "]: " + jsonObject.getString("message"));
                }
            } catch(Exception e) {
                flag = false;
                interrupt();
            }
        }
    }
}
