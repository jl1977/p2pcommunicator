package pl.kubalasek.p2p;

import java.io.StringWriter;
import java.net.Socket;
import javax.json.Json;
public class Peer {
    static Sender sender;
    static String username;

    /**
     * W tej metodzie tworzymy nowego peer'a
     * startujemy wątek serwera
     * */
    public static Peer create(String username, String port) throws Exception {
        System.out.printf("Create peer with %s : %s", username, port);
        System.out.println();
        Peer.username = username;
        sender = new Sender(port);
        sender.start();
        Peer peer = new Peer();
        return peer;
    }

    public Socket connectToPeer(String config) throws Exception {
        String[] setupValues = config.split(" ");
        return connectToPeer(setupValues[0], setupValues[1]);
    }
    /**
     * Metoda zwraca socket dla hostname i port
     * W nowym wątku  PeerThread startujemy socket
     * @throws Exception
     *         Jeżeli utworzenie socketu z jakiegoś powodu sie nie powiedzie
     *         informujemy użytkownika odpowiednim komunikatem
     *         Jeżeli uda się utworzyć socket, ale nie działa zamykamy go.
     *
     * */

    public Socket connectToPeer(String hostname, String port) throws Exception {
            Socket socket = null;
            try {
                socket = new Socket(hostname, Integer.valueOf(port));
                new Receiver(socket).start();
                System.out.println("Connected to "+hostname+" : "+port);
            } catch(Exception e) {
                if (socket != null) socket.close();
                else System.out.println("invalid input. skipping to next step.");

                System.out.println(e);
            }
        return socket;
    }
    /**
     * Metoda wysyłająca wiadomość
     * @param message
     *        parametr pobierany z pola addNewLine
     * StringWriter @see https://docs.oracle.com/javase/7/docs/api/java/io/StringWriter.html
     *
     * */


    public void sendMessage(String message){
        StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                            .add("username", username)
                            .add("message", message)
                            .build());
                    sender.sendMessage(stringWriter.toString());
    }


}

