package pl.kubalasek.p2p;

import pl.kubalasek.p2p.ui.Labels;
import pl.kubalasek.p2p.ui.Menu;
import pl.kubalasek.p2p.ui.TextField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Główne okno programu
 */
public class MainWindow extends JFrame implements ActionListener {
    Menu menu = new Menu(this);
    Labels label = new Labels();
    private static TextField textField = new TextField();

    Peer peer = null;
    String username = null;
    String port = null;

    /**
     * Konstruktor głownego okna programu
     * @param config
     *        W tym parametrze dostajemy hostname i port
     *        z okno loginWindow
     *
     * */

    public MainWindow(String config){
        setTitle("Peer to peer communicator");
        setSize(485,485);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setVisible(true);
        setJMenuBar(menu.getMenuBar());
        add(label.getConfirmUsernameAndPort());
        add(textField.getUsernameAndPort());
        add(textField.getConversation());
        add(textField.getAddNewLine());

        setConfigData(config);
        textField.getUsernameAndPort().addActionListener(registerAction);
        textField.getAddNewLine().addActionListener(sendMessage);
    }



    /**
     * Ustawiamy dwa listenery na pola z nazwą użytkownika i nową wiadomościa
     * @param registerAction
     *        parametr przekazywany z metody actionPerformed, podłączamy sie do peer
     *        z odpowiednia nazwą użytkownika i haslem, dodajemy do konwersacji informacje o podłaczeniu
     * @param sendMessage
     *        parametr przekazywany z metody actionPerformed, wysyłamy wiadomość pobraną z pola addNewLine,
     *        dodajemy wiadomość do konwersacji,
     *        czyścimy zawartość pola addNewLine
     *
     * */

    private void setConfigData(String config){
        String[] setupValues = config.split(" ");
        username = setupValues[0];
        port = setupValues[1];

        try {
            peer = Peer.create(username, port);
        }catch (Exception exception){
            System.out.println(exception);
        }
    }
    Action registerAction = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try {
                peer.connectToPeer(textField.getUsernameAndPort().getText());
                textField.getConversation().setText(textField.getConversation().getText()+"\nConnected to "+textField.getUsernameAndPort().getText());
            }catch (Exception exception){
                System.out.println(exception);
            }
        }
    };

    Action sendMessage = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try {
                peer.sendMessage(textField.getAddNewLine().getText());
                MainWindow.appendNewMessage("->"+textField.getAddNewLine().getText());
//                textField.getConversation().setText(textField.getConversation().getText()+"\n->"+textField.getAddNewLine().getText());
            }catch (Exception exception){
                System.out.println(exception);
            }
        }
    };
    @Override
    public void actionPerformed(ActionEvent e) {

        }

    /**
     * Metoda doklejająca nową wiadomość do konwersacji w oknie Conversation
     * Może to być wiadomośc wychodząca lub przychodząca
     * @param message
     *        tworzony z getText na AddNewLine (wiadmość wychodząca)
     *        z obiektu typu JSON na wątku Peer (wiadmość przychodząca)
     * */

        public static void appendNewMessage(String message){
            textField.getConversation().setText(textField.getConversation().getText()+"\n"+message);
        }

}
