package pl.kubalasek.p2p.ui;

import pl.kubalasek.p2p.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Menu extends JMenuBar implements ActionListener {
    JMenuBar menuBar;
    JMenu menuFile;
    JMenuItem openNewInstance;
    JMenuItem peerList;
    JMenuItem exit;
    JFrame frame;

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public Menu(JFrame frame){
        this.frame = frame;
        menuBar = new JMenuBar();
        menuFile = new JMenu("File");
        openNewInstance = new JMenuItem("New aplication window");
        openNewInstance.addActionListener(this::actionPerformed);
        peerList = new JMenuItem("List of peers available");
        peerList.addActionListener(this::actionPerformed);
        exit = new JMenuItem("Exit");
        exit.addActionListener(this::actionPerformed);
        menuBar.add(menuFile);
        menuFile.add(openNewInstance);
        menuFile.add(peerList);
        menuFile.add(exit);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == openNewInstance){
            ProcessBuilder pb = new ProcessBuilder("C:\\Program Files\\Java\\jdk-13.0.1\\bin\\java.exe",
                    "-jar", "C:\\Development\\Projekty\\Java\\P2Ptest\\out\\artifacts\\P2Ptest_jar\\P2Ptest.jar");
            pb.directory(new File("C:\\Development\\Projekty\\Java\\P2Ptest"));
            try {
                Process p = pb.start();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if(source == exit){
            frame.dispose();
        }
    }


}
