package pl.kubalasek.p2p.ui;

import javax.swing.*;

public class TextField extends JTextField {
    JTextField usernameAndPort, addNewLine;
    JTextArea conversation;

    public JTextField getUsernameAndPort() {
        return usernameAndPort;
    }

    public JTextArea getConversation() {
        return conversation;
    }

    public JTextField getAddNewLine() {
        return addNewLine;
    }

    public  TextField(){
        usernameAndPort = new JTextField();
        usernameAndPort.setBounds(230, 20, 200, 20);
        conversation = new JTextArea();
        conversation.setBounds(10, 50, 450, 300);
        addNewLine = new JTextField();
        addNewLine.setBounds(10, 360, 450, 40);
    }
}
