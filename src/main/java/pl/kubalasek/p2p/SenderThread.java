package pl.kubalasek.p2p;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
public class SenderThread extends Thread {
    private Sender sender;
    private Socket socket;
    private PrintWriter printWriter;

    /**
     * W konstruktorze inicializujemy socket i serverThread
     * */
    public SenderThread(Socket socket, Sender sender) {
        this.sender = sender;
        this.socket = socket;
    }
    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);
            while(true) sender.sendMessage( bufferedReader.readLine());
        } catch (Exception e) { sender.getSenderThreads().remove(this); }
    }
    /**
     *
     * Getter dla printWriter'a
     * Wykorzystywany przy wysyłaniu wiadomości do peerów
     */
    public PrintWriter getPrintWriter() { return printWriter; }
}
